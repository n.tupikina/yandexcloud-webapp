provider "docker" {
}

resource "docker_image" "webapp" {
  name         = "webapp:latest"
  keep_locally = true
}

resource "docker_container" "webapp" {
  name  = "webapp"
  image = docker_image.webapp.image_id

  ports {
    external = 8080
    internal = 80
  }
}
