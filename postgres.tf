resource "yandex_mdb_postgresql_cluster" "postgrescluster" {
  name        = "test"
  environment = "PRESTABLE"
  network_id  = yandex_vpc_network.postgres.id

  config {
    version = 15
    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = 16
    }
    postgresql_config = {
      max_connections                   = 10
      enable_parallel_hash              = true
      autovacuum_vacuum_scale_factor    = 0.34
      default_transaction_isolation     = "TRANSACTION_ISOLATION_READ_COMMITTED"
      shared_preload_libraries          = "SHARED_PRELOAD_LIBRARIES_AUTO_EXPLAIN,SHARED_PRELOAD_LIBRARIES_PG_HINT_PLAN"
    }
  }

  maintenance_window {
    type = "WEEKLY"
    day  = "SAT"
    hour = 12
  }

  host {
    zone      = "ru-central1-a"
    subnet_id = yandex_vpc_subnet.postgres.id
  }
}

resource "yandex_vpc_network" "postgres" {}

resource "yandex_vpc_subnet" "postgres" {
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.postgres.id
  v4_cidr_blocks = [var.my_ip]
}

resource "yandex_mdb_postgresql_user" "dbuser" {
  cluster_id = yandex_mdb_postgresql_cluster.postgrescluster.id
  name       = var.db_user
  password   = var.db_password
  depends_on = [yandex_mdb_postgresql_cluster.postgrescluster]
}

resource "yandex_mdb_postgresql_database" "postgres" {
  owner      = yandex_mdb_postgresql_user.dbuser.name
  cluster_id = yandex_mdb_postgresql_cluster.postgrescluster.id
  name       = var.db_name
  depends_on = [yandex_mdb_postgresql_cluster.postgrescluster]
}
