FROM python:3

RUN apt update
RUN apt install -y python3-tk
RUN pip3 install requests python-dotenv psycopg2 Flask

RUN mkdir /app
COPY .env /root/
COPY checking.py /root/
COPY checker.py /root/

ENTRYPOINT ["python3", "./checking.py"]
CMD [ "python3", "./checker.py" ]
