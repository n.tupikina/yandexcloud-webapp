from flask import Flask
import requests 
import psycopg2
from dotenv import load_dotenv
import os
import time

load_dotenv()
app = Flask(__name__)
conn = psycopg2.connect(database="postgres",
                        host=os.getenv("IP_DB"),
                        user=os.getenv("USER_POST"),
                        password=os.getenv("PASSWORD_POST"),
                        port=5433)

@app.route('/') 
def check_cadepo():
    cursor = conn.cursor()
    query="SELECT * FROM avav WHERE id=(SELECT max(id) FROM avav);"
    cursor.execute(query)
    result=cursor.fetchone()
    cursor.close()
    return 'status is '+str(result[1])+' and the most recent response time is '+str(result[2][9:])+' ms'

@app.route('/status')
def cadepo_status():
    times=[]
    fail=[]
    cursor = conn.cursor()
    query='''SELECT * FROM (
  SELECT * FROM avav ORDER BY id DESC LIMIT 10
) as r ORDER BY id;'''
    cursor.execute(query)
    result=cursor.fetchall()
    for i in result:
        times.append(int(i[2][9:]))
    if i[1]!=200:
            fail.append(1)
    resp=sum(times)/len(times) 
    fail_resp=len(fail)/len(result)   
    return f"average response time during last 5 minutes is {resp} ms and the failure rate currently is {fail_resp}"

@app.route('/uptime')
def cadepo_up():
    success=[]
    cursor = conn.cursor()
    query='''SELECT * FROM avav;'''
    cursor.execute(query)
    result=cursor.fetchall()
    for i in result:
        if i[1]==200:
            success.append(1)
    resp=len(success)/len(result)
    resp=resp*100
    return "server uptime is %s percent" % resp



if __name__ == '__main__': 
    app.run(host='0.0.0.0', debug=True)
