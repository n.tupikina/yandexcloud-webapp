import requests 
import psycopg2
import time
from dotenv import load_dotenv
import os

load_dotenv()
conn = psycopg2.connect(database="postgres",
                        host=os.getenv("IP_DB"),
                        user=os.getenv("USER_POST"),
                        password=os.getenv("PASSWORD_POST"),
                        port=5433)

while True:
    response = requests.get('https://cadepo.pro/')
    ret=response.status_code
    tim=response.elapsed
    cursor = conn.cursor()

    query="Select * from avav;"
    try:
        cursor.execute(query)
        result=cursor.fetchone()
    except:
        table_creation = (
            """ CREATE TABLE avav (
                id SERIAL PRIMARY KEY,
                status INT NOT NULL,
                time VARCHAR(255) NOT NULL
                );
            """
            )   
        cursor.execute(table_creation)
        conn.commit()
    
    sql = "INSERT INTO avav (status, time) VALUES (%s, %s)"
    #sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
    val = (ret, tim)
    cursor.execute(sql, val)
    conn.commit()
    cursor.close()
    
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM avav;")
    cursor.close()
    time.sleep(30)