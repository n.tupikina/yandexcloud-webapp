terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.id_for_cloud
  folder_id = var.id_for_folder
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "default" { 
  name = "test-instance"
	platform_id = "standard-v1"

  resources {
    core_fraction = 2
    cores  = 2 
    memory = 1 
  }

  boot_disk {
    initialize_params {
      image_id = var.id_of_subnet 
    }
  }

  network_interface {
    subnet_id = var.id_of_subnet
    nat = true 
  }
  
  scale_policy {
    fixed_scale {
      size = 2
    }
  }
  
  deploy_policy {
    max_unavailable = 1
    max_creating    = 1
    max_expansion   = 1
    max_deleting    = 1
  }

  application_load_balancer {
    target_group_name = "alb-tg"
  }
}

